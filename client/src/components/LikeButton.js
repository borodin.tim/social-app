import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { gql, useMutation } from '@apollo/client';
import { Button, Icon, Label } from 'semantic-ui-react';

import InvertedDelayedPopup from '../utils/InvertedDelayedPopup';

function LikeButton({ user, post: { id, likeCount, likes } }) {
    const [liked, setLiked] = useState(false);

    useEffect(() => {
        if (user && likes.find(like => like.username === user.username)) {
            setLiked(true);
        } else {
            setLiked(false);
        }
    }, [user, likes]);

    const [likePost] = useMutation(LIKE_POST_MUTATION, {
        variables: { postId: id }
    });

    const likeButton = user ? (
        liked ? (
            <Button color="orange">
                <Icon name="heart" />
            </Button>
        ) : (
                <Button color="orange" basic>
                    <Icon name="heart" />
                </Button>
            )
    ) : (
            <Button as={Link} to="/login" color="orange" basic>
                <Icon name="heart" />
            </Button>
        )

    return (

        <Button as="div" labelPosition="right" onClick={likePost}>
            <InvertedDelayedPopup content={liked ? "Unlike" : "Like"}>
                {likeButton}
            </InvertedDelayedPopup>
            <Label basic color="orange" pointing="left">
                {likeCount}
            </Label>
        </Button>
    );
}

const LIKE_POST_MUTATION = gql`
    mutation likePost($postId: ID!){
        likePost(postId: $postId){
            id
            likes{
                id
                username
            }
            likeCount
        }
    }`;

export default LikeButton;