import React from 'react';
import { Button, Form } from 'semantic-ui-react';
import { gql, useMutation } from '@apollo/client';

import { useForm } from '../utils/hooks';
import { FETCH_POSTS_QUERY } from '../utils/graphql';

export default function PostForm() {
    const { values, onChange, onSubmit } = useForm(createPostCollback, {
        body: ''
    });

    const [createPost, { error }] = useMutation(CREATE_POST_MUTATION, {
        variables: values,
        update(proxy, result) {
            const data = proxy.readQuery({
                query: FETCH_POSTS_QUERY
            });
            const newData = { getPosts: [result.data.createPost, ...data.getPosts] };
            proxy.writeQuery({ query: FETCH_POSTS_QUERY, data: newData });
            values.body = '';
        }
    });

    function createPostCollback() {
        createPost();
    }

    return (
        <div>
            <Form onSubmit={onSubmit} autoComplete="off">
                <h2>Create a post</h2>
                <Form.Field>
                    <Form.Input
                        placeholder="Write your post"
                        name="body"
                        onChange={onChange}
                        value={values.body}
                        error={error ? true : false}
                    />
                    <Button type="submit" color="orange">
                        Submit
                    </Button>
                </Form.Field>
            </Form>
            {error && (
                <div className="ui error message" style={{ marginBottom: 20 }}>
                    <ul className="list">
                        <li>{error.graphQLErrors[0].message}</li>
                    </ul>
                </div>
            )
            }
        </div >
    )
}

const CREATE_POST_MUTATION = gql`
    mutation createPost($body: String!){
        createPost(body: $body){
            id 
            body
            createdAt
            username
            likes{
                id
                username
                createdAt
            }
            likeCount
            comments{
                id
                body
                username
                createdAt
            }
            commentCount
        }
    }
`;