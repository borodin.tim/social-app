import { Popup } from 'semantic-ui-react';
import React from 'react';

function InvertedDelayedPopup({ content, children }) {
    return <Popup inverted mouseEnterDelay={500} content={content} trigger={children} />
}

export default InvertedDelayedPopup;