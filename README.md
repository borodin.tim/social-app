# Simple social app.

## Stack used: MERNG

- mongoDB
- express
- react
- nodejs
- graphql

### To run:

- npm run serve
- cd client, npm start

### TODO:

1. Add Popup over user icon
2. Investigate why Loggedout user gets errors when trying to like a post
3. Deploy the app onto Heroku

### Future Ideas:

- Email integration (forgot password, confirm account)
- Profile View (user's can click on each others profiles and maybe "follow" them)
- Profile Edits (users can upload information about themselves: email, website, "about me", etc.)
- Edit posts (users can delete their post, but not edit them. Add an "edit" feature.)
- File uploads (allow users to upload images, maybe videos, etc.)
- Change the styling, make it your own
